# nlptd4

TD 4 - NLP Class
Dec 2020

**Authors :** CHRETIEN Jérémy, DAVIDSON Colin, LAFAGE Adrien, REMBUSCH Gabrielle and WILBRINK Aurore.

## Data's source

You can find the source of the dataset that we use [here](https://ai.stanford.edu/~amaas/data/sentiment/).

## Dataset

To load both train and test datasets in your python script, you may use the python library Pandas:

```python
import pandas

train_set = pd.read_csv("data/train_dataset.csv")
test_set = pd.read_csv("data/test_dataset.csv")
```

This will give you two `pandas.DataFrame` with a label and a features columns.

**Example:**

|label|features|
|-|-|
|'pos'|'a positive movie review...'|
|'neg'|'a negative movie review...'|
|...|...|

## Structure

* **data** :
  * *cleaned_test_dataset.csv*
  * *cleaned_train_dataset.csv*
  * *test_dataset.csv*
  * *train_dataset.csv*
* *lda_ipynb* : notebook applying latent dirichlet allocation.
* *preprocessing.ipynb* : notebook preprocessing the datasets `test_dataset.csv` and `train_dataset.csv`, and saving the results in `cleaned_test_dataset.csv` and `cleaned_train_dataset.csv`.
* *stopwords.py* : list of words we judged good to remove from the vocabulary for training our models.
* *training_ml.ipynb* : notebook training naives bayes and random forest classifiers.
* *training_rnn.ipynb* : notebook training a recurrent neural network.
